<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ChangePasswordSuccess</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>93c3c9bc-1a81-4a93-a978-175fa2cb596a</testSuiteGuid>
   <testCaseLink>
      <guid>5661a435-ec04-4fd7-9bf8-b080ef5eced0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Failed to change password where the length exceeds 25</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>84516ad8-7558-4297-b5e6-4e251e53b132</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Failed to change password with a length less than 8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fc8c2de8-956d-4890-b5cb-e006522f20c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Failed to change password without letters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44c563a5-19eb-4024-b84c-8665ac85bec7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Failed to change password without numbers</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa5d905a-9a93-4419-92e7-699b97350843</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Failed to change password without numbers</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>abbcdce7-e13e-47a4-b860-57efcf279392</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Failed to change password without special characters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dafb8aca-6201-45ec-9c91-328922671e53</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login fail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ee5ddc80-737b-48df-830a-4de274a2eca0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Logout success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b6b39f0-ce11-4460-b2d6-57176a0b80ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Logout success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c04aebb7-0802-44d8-8f4b-6e8f60345a33</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Password change successfully</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
