<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_(Profile)                         (Citi_d63da0</name>
   <tag></tag>
   <elementGuidId>c932b189-da54-48de-908f-76ab06bcf7e8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='page-wrapper']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#page-wrapper</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>page-wrapper</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
                
                	
ข้อมูลส่วนบุคคล (Profile)


    
        
            หมายเลขบัตรประชาชน (Citizen ID)1104300300329
            รหัสนิสิต (Student code)
          62160311
        
        
          ชื่อ - นามสกุล (Name)
          SIRARAT YAIMADEUA
          คณะ (Faculty)Informatics
        
        
          รหัสผ่านหมดอายุ (Password Expire)2022-09-06 06:23:25 (balance : 179 days)          
          บัญชีผู้ใช้หมดอายุ (Account Expire)2572-06-22 02:00:52
        
    

 

 


                    
                    
                
                
                
            
        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page-wrapper&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='page-wrapper']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='wrapper']/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'page-wrapper' and (text() = '
            
                
                	
ข้อมูลส่วนบุคคล (Profile)


    
        
            หมายเลขบัตรประชาชน (Citizen ID)1104300300329
            รหัสนิสิต (Student code)
          62160311
        
        
          ชื่อ - นามสกุล (Name)
          SIRARAT YAIMADEUA
          คณะ (Faculty)Informatics
        
        
          รหัสผ่านหมดอายุ (Password Expire)2022-09-06 06:23:25 (balance : 179 days)          
          บัญชีผู้ใช้หมดอายุ (Account Expire)2572-06-22 02:00:52
        
    

 

 


                    
                    
                
                
                
            
        ' or . = '
            
                
                	
ข้อมูลส่วนบุคคล (Profile)


    
        
            หมายเลขบัตรประชาชน (Citizen ID)1104300300329
            รหัสนิสิต (Student code)
          62160311
        
        
          ชื่อ - นามสกุล (Name)
          SIRARAT YAIMADEUA
          คณะ (Faculty)Informatics
        
        
          รหัสผ่านหมดอายุ (Password Expire)2022-09-06 06:23:25 (balance : 179 days)          
          บัญชีผู้ใช้หมดอายุ (Account Expire)2572-06-22 02:00:52
        
    

 

 


                    
                    
                
                
                
            
        ')]</value>
   </webElementXpaths>
</WebElementEntity>
